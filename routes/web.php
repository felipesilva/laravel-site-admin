<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index');

Route::resource('fotos', 'FotosController');

Route::get('/pesquisar', 'ClienteController@buscaHome');


Auth::routes();
Route::get('/painel', ['middleware' => 'auth','uses' => 'HomeController@index']);

Route::get('/logout','HomeController@logout');


//website
Route::get('/contato',function(){
    return view("website/contato");
});


//Route::get('/home', 'HomeController@index')->name('home');

//Route::resource('cliente', 'ClienteController')->middleware('auth');

Route::get('/cliente/{id}', function($id){
    return App::make('App\Http\Controllers\ClienteController')->show($id);
});

//Route::get('/create', function(){
//    return view('cliente/create');
//})->middleware('auth');



Route::get('/cliente','ClienteController@index')->middleware('auth');
Route::get('/cliente/{id}/edit','ClienteController@edit')->middleware('auth');

Route::patch('/cliente/{id}','ClienteController@update')->middleware('auth');
Route::post('/cliente','ClienteController@store')->middleware('auth');

Route::get('/create','ClienteController@create')->middleware('auth');

Route::delete('/cliente/{id}','ClienteController@destroy')->middleware('auth');



Route::post('/upload','FotosController@upload');

//all clientes
Route::get('/todos','ClienteController@showall');