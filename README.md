

<p>Comando para criar migration,resources, controllers, models e views:</p>
<blockquote>
php artisan resource-file:create AssetCategory --fields="id,name,description,is_active"
</blockquote>
<blockquote>
php artisan create:resources AssetCategory
</blockquote>